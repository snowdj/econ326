rm(list=ls())
graphics.off()
require(ggplot2)
N <- 1000

x <- runif(N)*5
e <- rnorm(N)
y <- 0.25 + 0.5*x + e*.25
df <- data.frame(x,y)
## homoskedastic
pdf("homoskedastic.pdf")
p <- ggplot(data = df, aes(x = x, y = y)) +
  geom_smooth(method = "lm", se=FALSE, color="red", formula = y ~ x) +
  geom_point() + scale_y_continuous(limits=c(-0.5,4))
p
dev.off()
df$y <- 0.25 + 0.5*x + e*.25*(abs(x-1)+.05)
pdf("heteroskedastic.pdf")
p <- ggplot(data = df, aes(x = x, y = y)) +
  geom_smooth(method = "lm", se=FALSE, color="red", formula = y ~ x) +
  geom_point() + scale_y_continuous(limits=c(-0.5,4))
p
dev.off()



