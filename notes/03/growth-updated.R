rm(list=ls())
graphics.off()
require(ggplot2)
## For labeling, see http://stackoverflow.com/a/13451587
lm_eqn = function(m) {
  l <- list(a = format(coef(m)[1], digits = 2),
            b = format(abs(coef(m)[2]), digits = 2))
  if (coef(m)[2] >= 0)  {
    eq <- substitute(italic(y) == a + b %.% italic(x),l)
  } else {
    eq <- substitute(italic(y) == a - b %.% italic(x),l)
  }

  as.character(as.expression(eq));
}

## Download GDP data from World Bank
if (!file.exists("gdp.zip")) {
  download.file("http://api.worldbank.org/v2/en/indicator/ny.gdp.pcap.cd?downloadformat=csv",
                destfile="gdp.zip")
}
con <- unz("gdp.zip", "b0766ddb-00d8-4933-9c1a-60da26b3c3e5_v2.csv")
gdp <- read.csv(con, skip=4)

# create an indicator for things that are not countries but groups of country
gdp$group <-
  grepl("EAP|EAS|ECA|ECS|FCS|HPC|INX|NOC|NAC|SAS|LAC|LCN|LDC|LIC|MEA|MNA|SSA|SSF|UMC|OEC|OED|OSS|EUU|HIC",gdp$Country.Code )

## theme for graphics
fontFamily <- "Helvetica"
themeLE <- function(base_size=12, base_family=fontFamily) {
  theme_bw() %+replace%
  theme(axis.line = element_line(colour="grey"),
        panel.grid.minor = element_blank(),
        panel.background= element_blank(),
        panel.grid.major.x = element_blank(),
        panel.background = element_rect(fill="white",colour =NA),
        panel.border=element_blank(),
        text = element_text(family=base_family, size=base_size,
          face="plain", hjust=0.5, vjust=0.5, angle=0, lineheight=0.9,
          colour="black", margin=margin(), debug=FALSE),
        axis.text.x = element_text(angle=60),
        legend.position="bottom")
}
fdims <-1.5*c(4, 3) ## figure dimensions
printDev <- function(filename, width=fdims[1], height=fdims[2], family=fontFamily) {
  cairo_pdf(filename,width=width,height=height,family=family)
  #pdf(filename,width,height)
}


plotGrowth <- function(year.start,year.end, groups=FALSE,
                       regression=FALSE, confidence.bands=FALSE) {
  df <- data.frame(Country.Name=gdp$Country.Name, growth=NA,gdp.start=NA,group=gdp$group)
  df$growth <-  (gdp[,sprintf("X%d",year.end)] /
                 gdp[,sprintf("X%d",year.start)]) ^(1/(year.end-year.start))- 1
  df$gdp.start <- gdp[,sprintf("X%d",year.start)]
  if (groups) {
    df <- subset(df,group)
  } else {
    df <- subset(df,!group)
  }
  p <- ggplot(data = df, aes(x = gdp.start, y = growth)) +
    geom_text(aes(label=Country.Name),size=4,hjust=0.5,vjust=1,angle=45) +
    geom_point(aes(),colour="red") +
    xlim(0,max(subset(df,!is.na(df$growth))$gdp.start,na.rm=TRUE)) +
    themeLE()
  if (regression) {
    l.df <- data.frame(x=quantile(df$gdp.start,0.9,na.rm=TRUE),y=quantile(df$growth,0.99,na.rm=TRUE))
    reg <- lm(growth ~ gdp.start, data=df)
    p <-  p +  geom_smooth(method = "lm", se=confidence.bands, color="red", formula = y ~ x) +
      geom_text(data=l.df,aes(x=x, y=y),
                label=lm_eqn(reg), parse=TRUE) +
                geom_point(aes(),colour="red") + themeLE() +
      xlab(sprintf("GDP in %d", year.start)) +
      ylab(sprintf("Growth from %d to %d",year.start,year.end))
    p <- list(p,reg)
  }
  p
}

## Make some fancy interactive web graphics

## Uses the "rCharts" package, which is not part of CRAN, but instead
## must be installed from github. Uncommment the following two lines
## to install it.

#library(devtools)
#install_github('rCharts', 'ramnathv')
library(rCharts)
y0 <- c(1960,1980,1995)
y1 <- c(1980,1995,2014)
df <- data.frame(Country=gdp$Country.Name, group=gdp$group)
for (s in y0) {
  for (e in y1) {
    if (e>s) {
      df[,sprintf("growth%d_%d",s,e)] <-  round((gdp[,sprintf("X%d",e)] /
                                                 gdp[,sprintf("X%d",s)])
                                                ^(1/(e-s))- 1,
                                                digits=3)
    }
  }
  df[,sprintf("gdp%d",s)] <- round(gdp[,sprintf("X%d",s)], digits=2)
}
df <- subset(df,!group)

p <- nPlot(growth1960_2014 ~ gdp1960, data=df, type="scatterChart")
#p$chart(showControls=FALSE) # hide magnify button
p$chart(tooltipContent = "#! function(key, x, y, e){
     return e.point.Country
     } !#")
p$addControls("x", value = "gdp1960", values = names(df)[grep("gdp",names(df))])
p$addControls("y", value = "growth1960_2014", values = names(df)[grep("growth",names(df))])
p
