rm(list=ls())
library(leafletR)
library(rgdal)
## location of cities from NOAA
## http://www.nws.noaa.gov/geodata/catalog/national/data/ci08au12.zip
cities <- readOGR("map","ci08au12")
proj4string(cities)<-
  CRS("+proj=merc +lon_0=0 +k=1 +x_0=0 +y_0=0 +a=6378137 +b=6378137 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs")

## merge with crime data
library(foreign)
mldata <- read.dta("mccrary-crime/data/crime2.dta")
mldata <- subset(mldata,year>=1970 & year<=1992) ## get rid of years without crime data
crimes <-
  c("murder","rape","robbery","assault","burglary","larceny","auto")
for(c in crimes) {
  mldata[,paste(c,"rate",sep=".")] <- mldata[,c]/mldata$citypop*1e5
}
mldata$police.per100k <- mldata$sworn/mldata$citypop*1e5

## cost of crime for each type from RAND
## http://www.rand.org/jie/centers/quality-policing/cost-of-crime.html
crimeWeights <- c(8649216, 217866, 67277, 87238, 13096, 2139, 9079)
names(crimeWeights) <- crimes
foo <- as.matrix(mldata[,crimes]) %*% as.vector(crimeWeights)
mldata$crime <- as.numeric(foo)
mldata$violent.crime <- as.numeric(as.matrix(mldata[,crimes]) %*%
  as.vector(crimeWeights*c(1,1,1,1,0,0,0)))
mldata$property.crime <- as.numeric(as.matrix(mldata[,crimes]) %*%
  as.vector(crimeWeights*(1-c(1,1,1,1,0,0,0))))
## crime is a cost weighted sum of different crimes
mldata$crime.rate <- mldata$crime/mldata$citypop
mldata$violent.crime.rate <- mldata$violent.crime/mldata$citypop
mldata$property.crime.rate <- mldata$property.crime/mldata$citypop


## merge together, first make names in location data match those in crime data
cities$ml.name <- tolower(cities$NAME)
cities$ml.name <- gsub(" ","",cities$ml.name)
cities$ml.name[cities$ml.name=="corpuschristi"] <- "corpuschri"
cities$ml.name[cities$ml.name=="oklahomacity"] <- "oklacity"
cities$ml.name[cities$ml.name=="phoenix"] <- "pheonix"
cities$ml.name[cities$ml.name=="saintpetersburg"] <- "saintpeters"
cities$ml.name[cities$ml.name=="sanfrancisco"] <- "sanfran"
cities$ml.name[cities$ml.name=="centercitycharlotte"] <- "charlotte"
cities$ml.name[cities$ml.name=="portlanddowntown"] <- "portland"
cities$ml.name[cities$ml.name=="downtowndetroit"] <- "detroit"
cities$ml.name[cities$ml.name=="centralelpaso"] <- "elpaso"
cities$ml.name[cities$ml.name=="downtownmemphis"] <- "memphis"
cities$ml.name[cities$ml.name=="st.louis"] <- "saintlouis"
cities$ml.name[cities$ml.name=="stpaul"] <- "saintpaul"
cities$ml.name[cities$ml.name=="losangeles" & cities$POP_1990==0] <-
  "ignore.this"
cities$ml.name[cities$ml.name=="seattle" & cities$POP_1990==0] <-
  "ignore.this"

cities$ml.name <- paste(cities$ml.name,tolower(cities$ST),sep=",")

mldata$statenam[mldata$name=="washington"] <- "dc"
mldata$name.st <- paste(mldata$name,mldata$statenam,sep=",")

##
merged <- merge(mldata,cities,by.x="name.st",by.y="ml.name",all.x=TRUE,all.y=FALSE)
library(data.table)
dt <- data.table(merged,key=c("name","year"))
dt[,sum(year>0),by=name]


out.dir <- "./map/geojson"
map.data <- merged[merged$year==1992,]
map.data$cr <- map.data$crime.rate
city.dat <- toGeoJSON(data=map.data, dest=out.dir,
                      lat.lon=c("LAT","LON"))

cuts<-round(quantile(map.data$cr, probs = seq(0, 1, 0.20), na.rm = TRUE))
cuts[1]<-0


sty<-styleGrad(prop="cr", breaks=cuts, right=FALSE,
               style.val=rev(heat.colors(5)), leg="crime rate")
q.map <- leaflet(data=city.dat, dest=out.dir,
                 style=sty,
                 incl.data=TRUE,
                 title="crime",
                 base.map="osm",
                 popup=c("name","crime.rate","murder.rate","violent.crime.rate","property.crime.rate"),
                 )



