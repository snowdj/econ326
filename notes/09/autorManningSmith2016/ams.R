load("pct.Rdata")
pct.state <- subset(pct.state,state!="DC") # drop Washington DC
library(ggplot2)

qplot(x=log(minw_cpi)-log(wage_p50), y=log(wage_p10)-log(wage_p50),
      data=pct.state) + theme_minimal(base_size=36)

## OLS
mod <- lm(I(log(wage_p10)-log(wage_p50)) ~
             I(log(minw_cpi)-log(wage_p50)) +
            I((log(minw_cpi)-log(wage_p50))^2) +
            as.factor(state) +
         as.factor(state)*year,
         data=pct.state)
marg.eff <- mod$coef[2] + 2*mod$coef[3] *
  mean(log(pct.state$minw_cpi)-log(pct.state$wage_p50))
cat(marg.eff,"\n")

## Estimates for all percentiles
library(multiwayvcov) # for clustered standard errors
library(lmtest) # for coeftest and waldtest
rhs <- "I(log(minw_cpi)-log(wage_p50)) +
  I((log(minw_cpi)-log(wage_p50))^2) + as.factor(state) +
         as.factor(state)*year + as.factor(year)"
ptiles <- seq(5,95,5)
marginal.effects <- function
(rhs, ptiles,
  estfun=function(p,rhs) {
    fmla <- as.formula(
        sprintf("I(log(wage_p%d)-log(wage_p50)) ~ %s",p,rhs))
    mod <- lm(fmla, data=pct.state)
    return(mod)
  }
)
{
  marg.eff <- rep(NA,length(ptiles))
  names(marg.eff) <- as.character(ptiles)
  marg.eff.se <- marg.eff
  for (p in ptiles) {
    mod <- estfun(p,rhs)
    coef.var <- cluster.vcov(mod, pct.state$state)
    marg.eff[as.character(p)] <- mod$coef[2] + 2*mod$coef[3] *
      mean(log(pct.state$minw_cpi)-log(pct.state$wage_p50))
    marg.eff.se[as.character(p)] <- sqrt(coef.var[2,2] +
    4*mean(log(pct.state$minw_cpi)-log(pct.state$wage_p50))^2*coef.var[3,3]
    +
    2*2*mean(log(pct.state$minw_cpi)-log(pct.state$wage_p50))*coef.var[2,3])
  }
  tab <- cbind(ptiles,marg.eff,marg.eff.se)
  colnames(tab) <- c("Percentile","Marginal Effect","SE")
  rownames(tab) <- NULL
  return(tab)
}
tab <- marginal.effects(rhs,ptiles)

fig <- ggplot(data=data.frame(tab),
              aes(x=Percentile,y=Marginal.Effect)) + geom_line() +
  geom_line(aes(y=Marginal.Effect+1.96*SE),linetype="dashed") +
  geom_line(aes(y=Marginal.Effect-1.96*SE),linetype="dashed") +
  theme_minimal(base_size=36)
fig

################################################################################
## Create table showing results with and without controls, etc
rhs <- list()
rhs[[1]] <- "I(log(minw_cpi)-log(wage_p50)) +
  I((log(minw_cpi)-log(wage_p50))^2)"
rhs[[2]] <- "I(log(minw_cpi)-log(wage_p50)) +
  I((log(minw_cpi)-log(wage_p50))^2) + as.factor(state) +
  as.factor(year)"
rhs[[3]] <- "I(log(minw_cpi)-log(wage_p50)) +
  I((log(minw_cpi)-log(wage_p50))^2) + as.factor(state) +
  as.factor(year) + as.factor(state)*year"
bigtab <- NULL
ptiles <- c(10,25,75,90)
for(r in rhs) {
  tab <- marginal.effects(r,ptiles)
  if (is.null(bigtab)) {
    bigtab <- tab
  } else {
    bigtab <- cbind(bigtab,tab[,2:3])
  }
}
## print a latex table
for (r in 1:nrow(bigtab)) {
  cat(sprintf("%d",bigtab[r,1]))
  for (c in 1:length(rhs)) {
    cat(sprintf(" & %.2f", bigtab[r,2*c]))
  }
  cat("\\\\ \n")
  for (c in 1:length(rhs)) {
    cat(sprintf(" & (%.2f)", bigtab[r,2*c+1]))
  }
  cat("\\\\ \n")
}

bigtab <- NULL
ptiles <- seq(5,95,5)
for(r in 1:length(rhs)) {
  tab <- marginal.effects(rhs[[r]],ptiles)
  if (is.null(bigtab)) {
    bigtab <- data.frame(tab,specification=as.character(r))
  } else {
    bigtab <- rbind(bigtab,data.frame(tab,specification=as.character(r)))
  }
}

fig <- ggplot(data=bigtab,
              aes(x=Percentile,y=Marginal.Effect,group=specification,colour=specification)) + geom_line() +
  geom_line(aes(y=Marginal.Effect+1.96*SE),linetype="dashed") +
  geom_line(aes(y=Marginal.Effect-1.96*SE),linetype="dashed") +
  theme_minimal(base_size=14)
fdims <-1.5*c(5.03937, 3.77953)
pdf("fig/ols-margeff.pdf",width=fdims[1],height=fdims[2])
fig
dev.off()



################################################################################
## 2SLS
library(AER)
## ivreg and multiwayvcov do not work correctly together with
## collinear regressors. The following code corrects the problem.
ivreg.fix <- function(mod) {
  if (all(is.na(mod$fitted.values))) {
    if ("x" %in% names(mod)) {
      x <- mod$x$regressors
    } else {
      stop("Must call ivreg with x=TRUE option")
    }
    coef <- mod$coef
    mod$fitted.values <- drop(x[,names(coef)[!is.na(coef)]] %*%
                              coef[!is.na(coef)])
    mod$residuals <- mod$y-mod$fitted.values
  }
  return(mod)
}
estfun.ivreg <- function(x) {
  if (all(is.na(x$fitted.values))) {
   x <- ivreg.fix(x)
  }
  xz <- model.matrix(x)
  xz <- naresid(x$na.action, xz)
  if (any(alias <- is.na(coef(x))))
    xz <- xz[, !alias, drop = FALSE]
  wts <- weights(x)
  if (is.null(wts))
    wts <- 1
  res <- residuals(x)
  rval <- as.vector(res) * wts * xz
  attr(rval, "assign") <- NULL
  attr(rval, "contrasts") <- NULL
  return(rval)
}
bread.ivreg <- function(x) {
  xz <- model.matrix(x)
  xz <- xz[,!is.na(x$coef)]
  return(solve(t(xz) %*% xz)*x$nobs)
}
## End of code to make ivreg work with collinear regressors
################################################################################

## First stage
first.stage <- lm(I((log(minw_cpi)-log(wage_p50))) ~
                    log(minw_cpi) + I(log(minw_cpi)^2) +
                    as.factor(state) +
                    as.factor(state):year + as.factor(year),
                  data=pct.state)
## check for relevance
coef.var <- cluster.vcov(first.stage, pct.state$state)
waldtest(mod,c(1,2),vcov=coef.var,test="F")

## 2SLS estimates
rhs <- "I(log(minw_cpi)-log(wage_p50)) +
  I((log(minw_cpi)-log(wage_p50))^2) + as.factor(year) + as.factor(state)*year"
instr <- "log(minw_cpi) + I(log(minw_cpi)^2) + as.factor(year) + as.factor(state)*year"
ptiles <- seq(5,95,5)
marg.eff <- rep(NA,length(ptiles))
names(marg.eff) <- as.character(ptiles)
marg.eff.se <- marg.eff
for (p in ptiles) {
  fmla <- as.formula(
      sprintf("I(log(wage_p%d)-log(wage_p50)) ~ %s | %s",p,
              rhs, instr))
  mod <- ivreg(fmla, data=pct.state,x=TRUE)
  coef.var <- cluster.vcov(mod, pct.state$state)
  marg.eff[as.character(p)] <- mod$coef[2] + 2*mod$coef[3] *
    mean(log(pct.state$minw_cpi)-log(pct.state$wage_p50))
  marg.eff.se[as.character(p)] <- sqrt(coef.var[2,2] +
    4*mean(log(pct.state$minw_cpi)-log(pct.state$wage_p50))^2*coef.var[3,3]
    +
    2*2*mean(log(pct.state$minw_cpi)-log(pct.state$wage_p50))*coef.var[2,3])
}
tab.iv <- cbind(ptiles,marg.eff,marg.eff.se)
colnames(tab.iv) <- c("Percentile","Marginal Effect","SE")
rownames(tab.iv) <- NULL

fig <- ggplot(data=data.frame(tab.iv),
              aes(x=Percentile,y=Marginal.Effect)) + geom_line() +
  geom_line(aes(y=Marginal.Effect+1.96*SE),linetype="dashed") +
  geom_line(aes(y=Marginal.Effect-1.96*SE),linetype="dashed") +
  theme_minimal(base_size=36)
fig


################################################################################
## Create table and figure showing results with and without controls, etc
rhs <- list()
rhs[[1]] <- "I(log(minw_cpi)-log(wage_p50)) +
  I((log(minw_cpi)-log(wage_p50))^2)"
rhs[[2]] <- "I(log(minw_cpi)-log(wage_p50)) +
  I((log(minw_cpi)-log(wage_p50))^2) + as.factor(state) +
  as.factor(year)"
rhs[[3]] <- "I(log(minw_cpi)-log(wage_p50)) +
  I((log(minw_cpi)-log(wage_p50))^2) + as.factor(state) +
  as.factor(year) + as.factor(state)*year"
bigtab <- NULL
ptiles <- c(10,25,75,90)
for(r in rhs) {
  tab <- marginal.effects(r,ptiles,
    function(p,rhs) {
      fmla <- as.formula(
      sprintf("I(log(wage_p%d)-log(wage_p50)) ~ %s | . - I(log(minw_cpi)-log(wage_p50)) -
  I((log(minw_cpi)-log(wage_p50))^2) + log(minw_cpi) + I(log(minw_cpi)^2)",p,
              rhs))
      mod <- ivreg(fmla,  data=pct.state, x=TRUE)
      return(mod)
    } )
  if (is.null(bigtab)) {
    bigtab <- tab
  } else {
    bigtab <- cbind(bigtab,tab[,2:3])
  }
}
## print a latex table
for (r in 1:nrow(bigtab)) {
  cat(sprintf("%d",bigtab[r,1]))
  for (c in 1:length(rhs)) {
    cat(sprintf(" & %.2f", bigtab[r,2*c]))
  }
  cat("\\\\ \n")
  for (c in 1:length(rhs)) {
    cat(sprintf(" & (%.2f)", bigtab[r,2*c+1]))
  }
  cat("\\\\ \n")
}

bigtab <- NULL
ptiles <- seq(5,95,5)
for(r in 1:length(rhs)) {
  tab <- marginal.effects(rhs[[r]],ptiles,
    function(p,rhs) {
      fmla <- as.formula(
      sprintf("I(log(wage_p%d)-log(wage_p50)) ~ %s | . - I(log(minw_cpi)-log(wage_p50)) -
  I((log(minw_cpi)-log(wage_p50))^2) + log(minw_cpi) + I(log(minw_cpi)^2)",p,
              rhs))
      mod <- ivreg(fmla,  data=pct.state, x=TRUE)
      return(mod)
    } )
  if (is.null(bigtab)) {
    bigtab <- data.frame(tab,specification=as.character(r))
  } else {
    bigtab <- rbind(bigtab,data.frame(tab,specification=as.character(r)))
  }
}

fig <- ggplot(data=bigtab,
              aes(x=Percentile,y=Marginal.Effect,group=specification,colour=specification)) + geom_line() +
  geom_line(aes(y=Marginal.Effect+1.96*SE),linetype="dashed") +
  geom_line(aes(y=Marginal.Effect-1.96*SE),linetype="dashed") +
  theme_minimal(base_size=14)
fdims <-1.5*c(5.03937, 3.77953)
pdf("fig/2sls-margeff.pdf",width=fdims[1],height=fdims[2])
fig + ylim(c(-0.75,1))
dev.off()
