\input{../../slideHeader}

\title{Example 2: The Contribution of the Minimum Wage to US Wage
  Inequality over Three Decades: A Reassessment}
\author{Paul Schrimpf}
\institute{UBC \\ Economics 326} 
\date{\today}


\providecommand{\bols}{{\hat{\beta}^{\mathrm{OLS}}}}
\providecommand{\biv}{{\hat{\beta}^{\mathrm{IV}}}}
\providecommand{\btsls}{{\hat{\beta}^{\mathrm{2SLS}}}}

\begin{document}

\frame{\titlepage}
%\setcounter{tocdepth}{2}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \tableofcontents  
\end{frame}

\begin{frame}\frametitle{Plan}
  \begin{itemize}
  \item Last week and today: how to use the methods we've studied 
  \item Today: another walk-through research project
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{References}
  \begin{itemize}
  \item ``The Contribution of the Minimum Wage to US Wage
    Inequality over Three Decades: A Reassessment'' \cite{ams2016}  
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

\section{Introduction}

\begin{frame}\frametitle{Wage percentiles in the US}
  \includegraphics[width=\textwidth]{fig/wage-percentiles-cpi}
\end{frame}

\begin{frame}\frametitle{Introduction}
  \begin{itemize}
  \item Inequality has been rising since the 1980s
  \item Why? 
    \begin{itemize}
    \item Rising demand for skills plus a slow-down in the supply of
      new college graduates
    \item Falling unionization \& decline in manufacturing
    \item \textbf{Falling real minimum wage}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Minimum wage}
  \includegraphics[width=\textwidth]{fig/minwage}
\end{frame}

\begin{frame}\frametitle{Wage percentiles \& the minimum wage}
  \includegraphics[width=\textwidth]{fig/wage-percentiles-cpi-minw}
\end{frame}

\begin{frame}\frametitle{Introduction}
  \begin{itemize}
  \item Two influential papers looked at impact of minimum wage on
    inequality
    \begin{itemize}
    \item \cite{dfl1996}: fall in minimum wage accounts for about half
      of increase in 50/10 wage inequality inequality increase from 1979-1988
    \item \cite{lee1999}: minimum wage accounts for more than all the
      increase in 50/10 wage inequality inequality increase from
      1979-1988
    \end{itemize}
  \item This paper: revisit the question with 20 more years of data
    with greater variation in minimum wage
  \end{itemize}
\end{frame}

\section{Data}
\begin{frame}[allowframebreaks]
  \frametitle{Data}
  \begin{itemize}
  \item Current Population Survey Merged Outgoing Rotation Group (CPS
    MORG) 
  \item 1979-2012
  \item Wage, age, state, gender
  \item State and federal minimum wage for each year
  \end{itemize} 
\end{frame}

\begin{frame}\frametitle{Data}
  \alert{Scenario}: we have gathered our data. What is the first thing
  we should do with it? 

  \vspace{12pt}

  We should check that it makes sense. Let's look at some tables and
  figures. What tables and figures should we create?
\end{frame}

\section{Empirical specification}

\begin{frame}\frametitle{Empirical specification}
  \begin{itemize}
  \item We want to estimate the effect of the minimum wage on inequality
  \item How to measure inequality?
  \item What equation should we estimate?
  \end{itemize}
\end{frame}

\begin{frame} \frametitle{Empirical specification}
  \begin{itemize}
  \item \cite{lee1999} specification:
    \begin{align*}
      \log w_{st}^{10} - \log w_{st}^{50} = & \beta_0 + \beta_1 (\log
      \min w_{st} - \log w_{st}^{50}) + \\ & + \beta_2 (\log
      \min w_{st} - \log w_{st}^{50})^2 + \\ & + \mathrm{controls} +
      \epsilon_{st} 
    \end{align*}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{OLS}
  \begin{itemize}
  \item What assumption do we need for OLS to be consistent?
  \item Suppose OLS is consistent, what is the predicted sign of
    $\bols_1$? 
  \item Why is OLS likely not consistent? In what direction do you
    think OLS will be biased?
  \end{itemize}
\end{frame}

%% Add question about measurement error and differencing?

\begin{frame}\frametitle{OLS - interpretation}
  How can we interpret the OLS estimates? Does the coefficient of
  interest have the expected sign? Is it small or large?
\end{frame}

\begin{frame}\frametitle{OLS - inference}
  How should we calculate standard errors? 
\end{frame}

\begin{frame}\frametitle{OLS - marginal effects}
  \begin{tabular}{l ccc}
    Percentile & \textbf{(1)} & \textbf{(2)} & \textbf{(3)} \\
    10 & 0.47 & 0.38 & 0.30\\ 
       & (0.03) & (0.03) & (0.02)\\ 
    25 & 0.15 & 0.10 & 0.07\\ 
       & (0.02) & (0.03) & (0.02)\\ 
    75 & -0.05 & 0.09 & 0.13\\ 
       & (0.02) & (0.02) & (0.02)\\ 
    90 & -0.12 & 0.19 & 0.21\\ 
       & (0.03) & (0.04) & (0.03)\\ 
    State Effects & No & Yes & Yes  \\
    Time Effects & No & Yes &  Yes  \\
    State-time trends & No & No & Yes  \\
  \end{tabular} 
\end{frame}

\begin{frame}\frametitle{OLS - marginal effects}
  \includegraphics[width=\textwidth]{fig/ols-margeff}
\end{frame}

\begin{frame}\frametitle{IV}
  We need an instrument for the effective minimum wage. What
  conditions does an instrument need to satisfy? What could be a good
  instrument?  
\end{frame}

\begin{frame}
  \frametitle{IV} The legislated minimum wage is a valid instrument
  if:
  \begin{enumerate}
  \item Relevant
  \item Exogenous
  \end{enumerate}
  Do these assumptions seem plausible? Can we check them?  
\end{frame}


\begin{frame}\frametitle{2SLS - specification}
  How should we specify the 2SLS estimator? What is the
  dependent variable(s)? What is the endogenous regressor of interest?
  What is the instrument? What controls should we include? 
  
  \vspace{12pt}
 
  What is the first stage? What is the reduced form? What should we
  check in these regressions? 
\end{frame}

\begin{frame}\frametitle{2SLS - interpretation and inference}
  How can we interpret the estimates? Does the coefficient of
  interest have the expected sign? Is it small or large?
  
  \vspace{12pt}
  
  How should we calculate standard errors? What hypothesis(es) should 
  we test?  
\end{frame}

\begin{frame}\frametitle{2SLS- marginal effects}
  \begin{tabular}{l ccc}
    Percentile & \textbf{(1)} & \textbf{(2)} & \textbf{(3)} \\
    10 & 0.16 & 0.22 & 0.12\\ 
               & (0.06) & (0.31) & (0.10)\\ 
    25 & -0.06 & 0.40 & 0.08\\ 
               & (0.04) & (1.64) & (0.09)\\ 
    75 & -0.01 & -0.38 & -0.02\\ 
               & (0.03) & (1.70) & (0.06)\\ 
    90 & -0.14 & -0.65 & 0.00\\ 
               & (0.05) & (2.71) & (0.07)\\ 
    State Effects & No & Yes & Yes  \\
    Time Effects & No & Yes &  Yes  \\
    State-time trends & No & No & Yes  \\
  \end{tabular} 
\end{frame}

\begin{frame}\frametitle{2SLS - marginal effects}
  \includegraphics[width=\textwidth]{fig/2sls-margeff}
\end{frame}


\begin{frame}\frametitle{2SLS - threats to validity}
  Why might the legislated minimum wage not be a valid instrument? Is there anything we
  can check to reassure us that the legislated minimum wage is exogenous?
\end{frame}

\begin{frame}\frametitle{Further results}
  Assuming we have correctly estimated the effect of the minimum wage
  on inequality what could we do with our estimate? What is a
  relevant policy question? What else do we need to know to answer
  that question?
\end{frame}


begin{frame}\frametitle{Code and data}
  \begin{itemize}
  \item
    \href{https://bitbucket.org/paulschrimpf/econ326/src/master/notes/09/autorManningSmith2016/ams.R?at=master}
    {Code for main results}
  \item
    \href{https://bitbucket.org/paulschrimpf/econ326/src/master/notes/09/autorManningSmith2016/ams-dataprep.R?at=master}
    {Code for preparing data}
  \item \href{./pct.Rdata}{Data set of wage percentiles and minimum
      wages in each state (created by ams-dataprep.R)}
  \end{itemize}
\end{frame}


\begin{frame}[allowframebreaks]
  \frametitle{References}
\bibliographystyle{jpe}
\bibliography{../../326}
\end{frame}
  
\end{document}


 