\input{../../slideHeader}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows}


\title{Example 2: Stimulating Local Public Employment: Do General Grants
    Work?}
\author{Paul Schrimpf}
\institute{UBC \\ Economics 326} 
\date{\today}


\providecommand{\bols}{{\hat{\beta}^{\mathrm{OLS}}}}
\providecommand{\biv}{{\hat{\beta}^{\mathrm{IV}}}}
\providecommand{\btsls}{{\hat{\beta}^{\mathrm{2SLS}}}}

\tikzstyle{block} = [rectangle, draw, fill=blue!20,
    text width=5em, text centered, rounded corners, minimum height=4em]
\tikzstyle{line} = [draw, very thick, color=black!50, -latex']

\graphicspath{{figures/}}

\begin{document}

\frame{\titlepage}
%\setcounter{tocdepth}{2}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \tableofcontents  
\end{frame}

\begin{frame}\frametitle{Plan}
  \begin{itemize}
  \item Last week and today: how to use the methods we've studied 
  \item Today: another walk-through research project
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{References}
  \begin{itemize}
  \item ``Stimulating Local Public Employment: Do General Grants
    Work?'' \cite{ldm2014}  
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

\section{Introduction}

\begin{frame}\frametitle{Introduction}
  \begin{itemize}
  \item Fiscal federalism: how to allocate expenditures and revenue
    among different levels of government
  \item Intergovernmental grants (usually from central to local
    governments) 
    \begin{itemize}
    \item Targeted
    \item General
    \end{itemize}
  \item Care about how grants affect employment because:
    \begin{itemize}
    \item Often used as stimulus to reduce unemployment
    \item Quality of public services 
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Fiscal federalism in Canada}
  \begin{tikzpicture}[node distance = 2cm, auto]
    \node [block] (federal) {federal};
    \node [block, below of=federal] (provincial) {provincial};
    \node [block, below of=provincial] (municipal) {municipal};
    % paths
    \path [line] (federal) -- node{Equalization payments, CHST, etc} (provincial);
    \path [line] (provincial) -- node{Targeted grants} (municipal);
  \end{tikzpicture}
\end{frame}

\begin{frame}\frametitle{Fiscal federalism in Sweden}
  \begin{itemize}
  \item Municipal public employment $\approx$ 20\% of work force
  \item Municipalities very autonomous
    \begin{itemize}
    \item Taxes, borrowing
    \item General grants 
    \end{itemize}
  \item Grants average 15\% of municipal revenue
    \begin{itemize}
    \item Per-capita, income-equalizing, and cost-equalizing 
    \item Cost-equalizing grants $ = \begin{cases} 
        100 \times (\% \Delta\text{pop} + 2) & \text{ if }
        \%\Delta \text{pop} \leq -2 \\
        0 & \text{ otherwise}\end{cases} + \text{other stuff}$
    \end{itemize}
  \end{itemize}
\end{frame}

\section{Data}
\begin{frame}[allowframebreaks]
  \frametitle{Data}
  \begin{itemize}
  \item 279 Swedish municipalities 1996-2004
  \item Variables:
    \begin{itemize}
    \item Municipal employment and wages by sector
      (admin, child care, schools, elderly care, social welfare, tech) 
    \item Cost-equalization grants 
    \item Demographics: population, portion young \& old, portion
      foreign
    \item Private employment in schools, child care, elderly care, and
      social welfare
    \end{itemize}
  \end{itemize} 
\end{frame}

\begin{frame}\frametitle{Data}
  \alert{Scenario}: we have gathered our data. What is the first thing
  we should do with it? 

  \vspace{12pt}

  We should check that it makes sense. Let's look at some tables and
  figures. What tables and figures should we create?
\end{frame}

\section{Empirical specification}

\begin{frame}\frametitle{Empirical specification}
  \begin{itemize}
  \item Want to estimate the effect of grants on employment
  \item What equation should we estimate
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{OLS}
  \begin{align*}
    \mathrm{emp}_{it} = & \beta_0 + \beta_1 g_{it} + \epsilon_{it}
  \end{align*}
  \begin{itemize}
  \item What assumption do we need for OLS to be consistent?
  \item Suppose OLS is consistent, what is the predicted sign of
    $\bols_1$? 
  \item Why is OLS likely not consistent? In what direction do you
    think OLS will be biased?
  \end{itemize}
\end{frame}

%% Add question about measurement error and differencing?

\begin{frame}\frametitle{OLS - interpretation}
  How can we interpret the OLS estimates? Does the coefficient of
  interest have the expected sign? Is it small or large?
\end{frame}

\begin{frame}\frametitle{OLS - inference}
  How should we calculate standard errors? 
\end{frame}

\begin{frame}\frametitle{IV}
  We need an instrument for . What conditions does an
  instrument need to satisfy? What could be a good instrument?

  \pause
  \vspace{12pt}
  
  We will use rule for allocating grants based on population change to
  form an instrument 
  \begin{align*}
    \text{grants} = & \begin{cases} 
        100 \times (\% \Delta\text{pop} + 2) & \text{ if }
        \%\Delta \text{pop} \leq -2 \\
        0 & \text{ otherwise}\end{cases} + \text{other stuff}
  \end{align*} 
  Define 
  \begin{align*}
    \text{migration grants} = \begin{cases} 
        100 \times (\% \Delta\text{pop} + 2) & \text{ if }
        \%\Delta \text{pop} \leq -2 \\
        0 & \text{ otherwise}\end{cases}
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{IV}
  Migration grants are a valid instrument if:
  \begin{enumerate}
  \item Relevant: migration grants correlated with cost-equalizing grants
  \item Exogenous: migration grants uncorrelated with unobservables
    affecting municipal employment
  \end{enumerate}
  Do these assumptions seem plausible? Can we check them?  
\end{frame}

\begin{frame} \frametitle{Exogeneity}
  \begin{itemize}
  \item Problem: migration grants $ = f(\text{\% population change})$,
    likely that municipal employment is also related to population
    (and its change)
  \item Solution: (Regression Kinked Design)
    \begin{itemize}
    \item Migration grants are a non-differentiable function of \%
      population change (there is a kink at -2)
    \item Assume that municipal employment is a smooth
      function of population
    \item Any kink in the relationship between employment and
      \% population change at -2 must be due to the kinked
      relationship between grants and \% population change
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}\frametitle{2SLS - specification}
  How should we specify the 2SLS estimator? What is the
  dependent variable(s)? What is the endogenous regressor of interest?
  What is the instrument? What controls should we include? 
  
  \vspace{12pt}
 
  What is the first stage? What is the reduced form? What should we
  check in these regressions? 
\end{frame}

% \begin{frame}\frametitle{Step 4: 2SLS - interpretation and inference}
%   How can we interpret the estimates? Does the coefficient of
%   interest have the expected sign? Is it small or large?
  
%   \vspace{12pt}
    
%   How should we calculate standard errors? What hypothesis(es) should 
%   we test?  
% \end{frame}

% \begin{frame}\frametitle{Step 4: 2SLS - threats to validity}
%   Why might elections not be a valid instrument? Is there anything we
%   can check to reassure us that elections are exogenous?
% \end{frame}

% \begin{frame}\frametitle{Step 5: further results}
%   Assuming we have correctly estimated the effect of police on crime,
%   what could we do with our estimate? What is a relevant policy
%   question? What else do we need to know to answer that question? 
% \end{frame}


\begin{frame}\frametitle{Code and data}
  \begin{itemize}
  \item
    \href{https://bitbucket.org/paulschrimpf/econ326/src/master/notes/09/ldm2014/ldm.R?at=master}
    {Code for main results}
  \item \href{https://bitbucket.org/paulschrimpf/econ326/src/master/notes/08/clusterFunctions.R?at=master}
    {Code for calculating heteroskedasticity robust clustered standard
      errors}
  \end{itemize}
\end{frame}


\begin{frame}[allowframebreaks]
  \frametitle{References}
\bibliographystyle{jpe}
\bibliography{../../326}
\end{frame}
  

\end{document}


