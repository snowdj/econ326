graphics.off()
library(rgl)

## plot with no collinearity
n <- 500
x1 <- rnorm(n)
x2 <- 3 - .3*x1 + rnorm(n) ## x2 is not a linear function of x1
                           ## because of added rnorm(n)
e <- rnorm(n)
y <- -1 + x1 + 0.5*x2 + e
reg <- lm(y ~ x1 + x2)

## create plot using Rgl
rgl.clear("all")
rgl.light()
bbox3d(xlab="x_1",ylab="y",zlab="x_2")
rgl.spheres(x1,y,x2,color="grey",radius=0.05, specular="white")
gx1 <- seq(min(x1),max(x1),len=5)
gx2 <- seq(min(x2),max(x2),len=5)
grid <- expand.grid(x1=gx1,x2=gx2)
grid$y <- predict(reg,newdata=grid)
rgl.surface(gx1, gx2, matrix(grid$y,nrow=5,ncol=5), color="blue",alpha=0.5,shininess=128)
aspect3d(x=1 ,y=96/128,z=1 )
#par3d(windowRect = c(100,100, 1280+100, 960+100) )
play3d(spin3d(axis=c(0,1,0),rpm=3))
#movie3d(spin3d(axis=c(0,1,0),rpm=10),duration=6,
#        fps=12,movie="notCollinear",convert=FALSE)

## plot with collinearity
n <- 500
x1 <- rnorm(n)
x2 <- 3 - .3*x1 ## x2 is perfectly linearly related to x1
e <- rnorm(n)
y <- -1 + x1 + x2 + e
reg <- lm(y ~ x1 + x2)
## create plot using Rgl
rgl.clear("all")
rgl.light()
bbox3d(xlab="x_1",ylab="y",zlab="x_2")
aspect3d(x=1 ,y=96/128,z=1 )
rgl.spheres(x1,y,x2,color="grey",radius=0.05, specular="white")
#gx1 <- seq(min(x1),max(x1),len=5)
#gx2 <- seq(min(x2),max(x2),len=5)
grid <- expand.grid(x1=gx1,x2=gx2)
grid$y <- predict(lm(y ~ I(x1-x2)),newdata=grid)
grid$y2 <- predict(lm(y ~ I(x1+0.2*x2)),newdata=grid)
rgl.surface(gx1, gx2, matrix(grid$y,nrow=5,ncol=5),
            color="blue",alpha=0.5,shininess=128)
rgl.surface(gx1, gx2, matrix(grid$y2,nrow=5,ncol=5),
            color="red",alpha=0.5,shininess=128)
aspect3d(x=1 ,y=96/128,z=1 )
play3d(spin3d(axis=c(0,1,0),rpm=1))
## par3d(windowRect = c(100,100, 1280+100, 960+100) )
## movie3d(spin3d(axis=c(0,1,0),rpm=10),duration=6,
##         fps=12,movie="collinear",convert=FALSE)
## ## movie3d writes to tempdir for some strange reason, so move the files
## system(paste("mv",paste(tempdir(),"*png",sep="/"),"figures/.",sep=" "))
## for(i in 0:9) {
##   ## animate in latex wants different file numbering, so
##   ## fix it
##   system(sprintf("mv figures/collinear00%d.png figures/collinear0%d.png",i,i))
##   system(sprintf("mv figures/notCollinear00%d.png figures/notCollinear0%d.png",i,i))
## }
