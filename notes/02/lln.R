rm(list=ls())   ## clear workspace
graphics.off()  ## close open graphs
n <- seq(1,1000) ## sample sizes
sims <- 5 ## number of simulations for each sample size
rand <- function(N) { return(runif(N)) } ## returns N random numbers

## simulation
sampleMean <- matrix(0,nrow=length(n),ncol=sims)
for(i in 1:length(n)) {
  N = n[i];
  x <- matrix(rand(N*sims),nrow=N,ncol=sims)
  sampleMean[i,] = colMeans(x)
}

## plot sample means
## If not installed, run:
##   install.packages("ggplot2")
##   install.packages("reshape")
library(ggplot2)
library(reshape)
dev.new()
df <- data.frame(sampleMean)
df$n <- n
df <- melt(df,id="n")
llnPlot <- ggplot(data=df,aes(x=n, y=value, colour=variable)) +
  geom_line() + scale_colour_brewer(palette="Paired") +
  scale_y_continuous(name="sample mean") +
  guides(colour=FALSE) + ## no legend
  theme_minimal()
print(llnPlot)

pdf("llnPlot.pdf")
print(llnPlot)
dev.off()
